# Maintainer: Martijn Braam <martijn@brixit.nl>
# Co-Maintainer: Luca Weiss <luca@z3ntu.xyz>
# Co-Maintainer: Bart Ribbers <bribbers@disroot.org>
# Co-Maintainer: Oliver Smith <ollieparanoid@postmarketos.org>
# Co-Maintainer: Clayton Craft <clayton@craftyguy.net>
_flavor=postmarketos-allwinner
_config="config-$_flavor.$CARCH"
pkgname=linux-$_flavor
pkgver=5.11.10_git20210325
pkgrel=0
_tag="orange-pi-5.11-20210325-1125"
pkgdesc="Kernel fork with Pine64 patches (megi's tree, slightly patched)"
arch="aarch64 armv7"
url="https://megous.com/git/linux/"
license="GPL-2.0-only"
makedepends="
	bison
	devicepkg-dev
	findutils
	flex
	gmp-dev
	installkernel
	mpc1-dev
	mpfr-dev
	openssl-dev
	perl
	rsync
	xz
	"
options="!strip !check !tracedeps pmb:cross-native pmb:kconfigcheck-anbox"
source="$pkgname-$_tag.tar.gz::https://github.com/megous/linux/archive/$_tag.tar.gz
	config-$_flavor.aarch64
	config-$_flavor.armv7
	0001-dts-add-dontbeevil-pinephone-devkit.patch
	0002-dts-add-pinetab-dev-old-display-panel.patch
	0003-dts-pinetab-add-missing-bma223-ohci1.patch
	0004-arm64-dts-allwinner-Add-bluetooth-node-to-the-PineTa.patch
	0005-dts-pinetab-make-audio-routing-consistent-with-pinep.patch
	0006-dts-pinephone-remove-bt-firmware-suffix.patch
	0007-media-ov5640-Implement-autofocus.patch
	0008-leds-gpio-Set-max-brightness-to-1.patch
	0009-dts-pinephone-Add-pine64-pinephone-to-compat-list.patch
	0010-dts-pinephone-drop-modem-power-node.patch
	0011-dts-pinephone-jack-detection.patch
	0012-pinephone-fix-pogopin-i2c.patch
	0013-drm-panel-simple-Add-Hannstar-TQTM070CB501.patch
	0014-ARM-dts-sun6i-Add-GoClever-Orion-70L-tablet.patch
	0015-drm-panel-simple-Add-Hannstar-HSD070IDW1-A.patch
	0016-ARM-dts-sun6i-Add-Lark-FreeMe-70.2S-tablet.patch
	"
subpackages="$pkgname-dev"
builddir="$srcdir/linux-$_tag"

case "$CARCH" in
	aarch64*) _carch="arm64" ;;
	arm*) _carch="arm" ;;
esac


prepare() {
	default_prepare

	REPLACE_GCCH=0 \
		. downstreamkernel_prepare
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION="$((pkgrel + 1 ))-$_flavor" \
		CFLAGS_MODULE=-fno-pic
}

package() {
	downstreamkernel_package "$builddir" "$pkgdir" "$_carch" "$_flavor"

	make -j1 modules_install dtbs_install \
		ARCH="$_carch" \
		INSTALL_MOD_STRIP=1 \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_DTBS_PATH="$pkgdir/usr/share/dtb"
}

dev() {
	provides="linux-headers"
	replaces="linux-headers"

	cd $builddir

	# https://github.com/torvalds/linux/blob/master/Documentation/kbuild/headers_install.rst
	make -j1 headers_install \
		ARCH="$_carch" \
		INSTALL_HDR_PATH="$subpkgdir"/usr
}

sha512sums="a57b8c168403c33ead5a1c963f60dc56add6233fa79496d8b97459c3003396708477b2f7888c9845e7a140d79397c7fc1ac726da2ca2104ca717207e387a28da  linux-postmarketos-allwinner-orange-pi-5.11-20210325-1125.tar.gz
2b632c867004f4f6bfa93cf7e2f0b04f63c8aabec2e6d1893e50f975f5c64d8301479080d950ada6e5e3f83ab08dfae644bdb98a20038ec672af92777d6ded96  config-postmarketos-allwinner.aarch64
3c222d5dd76f408e8a8122b5028a7e07431c9c2f02afc7ba76272b2d33f5b3be1df08ff80256b717d9c249abb52f237e086e250da215a5b0c854ddd45172ed03  config-postmarketos-allwinner.armv7
b174532e356dcb2144c2629c1747dcb0026a40aa8e4deca5bde67344d271e519ff90f6fa287633b1dec857855331e4aa1d1bd5fd0d73044309f98e8d829e2be0  0001-dts-add-dontbeevil-pinephone-devkit.patch
898e5e6dbdfa5b2f2154818bf006f2ae4d162a246c58dab5c43534e7630a35faf48ba74f6d2493d7500ad956a0e71c0b85ca9d7a51044c21d7d87b6f27b055f5  0002-dts-add-pinetab-dev-old-display-panel.patch
55f8bc826d7958d8fd16c59663746529be80daacdca9425e5678ee175bf0b93e4c1e0d2b5878ee58b488dde7eb7a05e438ec16c97c4298d3f272957a5caf183d  0003-dts-pinetab-add-missing-bma223-ohci1.patch
6135bffc2ab948169f6fb27b925ca64a4f2ad8cb58d66123f2bcb1c6e64ac68a92ab419bc1b19eaac64d508de46e4a2de639251f92f78596dece5941ded39e5d  0004-arm64-dts-allwinner-Add-bluetooth-node-to-the-PineTa.patch
f63ce49ef59fe2650d52e73e5ecbee3c2b2a3b4cb5c95703d19960a1775f7bf25e3f0ffee55201418019b911aa114277c0b04a8b19b8786ae4af4a734ea3d11a  0005-dts-pinetab-make-audio-routing-consistent-with-pinep.patch
e8eca7814daf76ebe658bdf988a062c4ed433cee5fde522b8d64492267716a5e7ae91b55877253ccedb44f5365b8a085989410a64371728189e444aee4f54655  0006-dts-pinephone-remove-bt-firmware-suffix.patch
22058627fd8e8d591d14d22e3ff5a19b8118e78f6bc620044ed68413fdac6cf1adeb77c75f4a2bae509b2294fe62b9c3fd18fb096e758dcd1bef47b515258ab5  0007-media-ov5640-Implement-autofocus.patch
662641bcb112d0568de3436269969d12a607820ea422554d8b7dfd3c5dbcef7e5ac60d47218a0d8a3c05044792b20ca22240a1007282472eade443670780b99a  0008-leds-gpio-Set-max-brightness-to-1.patch
95335f5755f8385c824cc8fb55af8b11ab02196ea6f49a80fe1ed964c0287fd6e41b597d50de615308e2cd53e0b0a471d452ea3ff8503caccfc17561c393c7f2  0009-dts-pinephone-Add-pine64-pinephone-to-compat-list.patch
e9cfb545c216c061da243b0eeb2169ff11e80898270a87f432cec937057d585313109d4ae9487a6641eeb5de1b0be69b7615bfca99fb64ab7227d941e6f129c1  0010-dts-pinephone-drop-modem-power-node.patch
789ae2e405f00e897c357427c587cb7a4a3d3a68b77b9700fb6f6a67d92314fcf75f2ad8b65c0d046215bb7bd5fcf65e4e8e31748170ed595bef1ba3010dd0bf  0011-dts-pinephone-jack-detection.patch
611bab79b36ee3a9ff8504fa5c56964a0ff1bcd65eacc692d8d991a9bc300e2b16c6d69f4bcad3a35315e848653c2969a985446c385081f019d3e4e74521f42c  0012-pinephone-fix-pogopin-i2c.patch
59f3da8cf071b2a26121dc4d84c3500e75a2d811067f514e7766fcacbef4ad469f7b89fe181098ae4f23eb39e0b60e1a071db58628c6b64b3e5e475504b2fd81  0013-drm-panel-simple-Add-Hannstar-TQTM070CB501.patch
cccc8e833cd2cd94a6dd79a287c81e4f5d8ad41876bc695afb44f241cdb609696466381adc135535e4db78a742c05181f2e29b6e750850f02241cac442dcdcd6  0014-ARM-dts-sun6i-Add-GoClever-Orion-70L-tablet.patch
59a508c9d4f3cfc68a7c2d9ce0fffcf5b4d213631f5909384719d2a1a5be7774c7aa971de404d8f4dafe4f0aa09f22a82688ca446226342a942715d511e14008  0015-drm-panel-simple-Add-Hannstar-HSD070IDW1-A.patch
9159c63ce65d2c7d9741ca65bd3e67ceaf95d4836d380660e4283fc8c0423e6e754e1d4d5c0b07ba9517e4072bc07ea39a6b4198ecfa00efb656617bcae68c9c  0016-ARM-dts-sun6i-Add-Lark-FreeMe-70.2S-tablet.patch"
